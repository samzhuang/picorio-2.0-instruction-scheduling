	.text
	.attribute	4, 16
	.attribute	5, "rv32i2p0"
	.file	"test.ll"
	.globl	mult                            # -- Begin function mult
	.p2align	2
	.type	mult,@function
mult:                                   # @mult
	.cfi_startproc
# %bb.0:
	addi	sp, sp, -16
	.cfi_def_cfa_offset 16
	sw	ra, 12(sp)                      # 4-byte Folded Spill
	.cfi_offset ra, -4
	call	__mulsi3@plt
	lw	ra, 12(sp)                      # 4-byte Folded Reload
	addi	sp, sp, 16
	ret
.Lfunc_end0:
	.size	mult, .Lfunc_end0-mult
	.cfi_endproc
                                        # -- End function
	.section	".note.GNU-stack","",@progbits
